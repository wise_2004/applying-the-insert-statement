-- Insert actors into the "actor" table
INSERT INTO public.actor (first_name, last_name)
VALUES
  ('Zuliya', 'Vosidova'),
  ('Jimm', 'Miller'),
  ('Linklin', 'Berrows');

-- Get the actor_ids of the newly inserted actors
 WITH inserted_actors AS (
   SELECT actor_id, first_name, last_name
   FROM public.actor
   WHERE (first_name, last_name) IN (
     ('Zuliya', 'Vosidova'),
     ('Jimm', 'Miller'),
     ('Linklin', 'Berrows')
   )
 )
 -- Associate actors with the film in the "film_actor" table
 INSERT INTO public.film_actor (actor_id, film_id)
 VALUES
   ((SELECT actor_id FROM inserted_actors WHERE first_name = 'Zuliya' AND last_name = 'Vosidova' LIMIT 1), (SELECT film_id FROM public.film WHERE title = 'LORD''S FAVORITE' LIMIT 1)),
   ((SELECT actor_id FROM inserted_actors WHERE first_name = 'Jimm' AND last_name = 'Miller' LIMIT 1), (SELECT film_id FROM public.film WHERE title = 'LORD''S FAVORITE' LIMIT 1)),
   ((SELECT actor_id FROM inserted_actors WHERE first_name = 'Linklin' AND last_name = 'Berrows' LIMIT 1), (SELECT film_id FROM public.film WHERE title = 'LORD''S FAVORITE' LIMIT 1));

