INSERT INTO public.film (film_id, title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features, fulltext)
VALUES (1001, 'LORD''S FAVORITE', 'A captivating story of Lord in a fictional world.', 2024, 1, 1, 14, 4.99, 120, 19.99, 'PG-13', ARRAY['Adventure', 'Fantasy'], to_tsvector('english', 'Lord''s Favorite A captivating story of Lord in a fictional world.'))
RETURNING film_id;

SELECT * FROM public.film WHERE title = 'LORD''S FAVORITE';
