-- Insert my favorite movies into the "film" table
INSERT INTO public.film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features, fulltext)
VALUES
  ('Prison Break', 'An exciting drama about escaping from prison.', 2005, 1, 3, 4.99, 60, 14.99, 'PG-13', '{Action, Drama}', to_tsvector('english', 'Prison Break An exciting drama about escaping from prison.')),
  ('Second War', 'A thrilling war movie set during World War II.', 2010, 1, 3, 4.99, 120, 19.99, 'R', '{Action, War}', to_tsvector('english', 'Second War A thrilling war movie set during World War II.'));
  -- Add more movies as needed;

-- Get the film_ids of the newly inserted movies
WITH inserted_movies AS (
  SELECT film_id
  FROM public.film
  WHERE title IN ('Prison Break', 'Second War')
)
-- Insert the movies into the "inventory" table (assuming store_id is 1)
INSERT INTO public.inventory (film_id, store_id, last_update)
VALUES
  ((SELECT film_id FROM inserted_movies WHERE title = 'Prison Break'), 1, now()),
  ((SELECT film_id FROM inserted_movies WHERE title = 'Second War'), 1, now());
  